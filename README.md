# Tactigen Catalog

In Rancher, this catalog contains the officially supported platforms and services available within Tactigen managed environments.  The sorce [umc-catalog](https://gitlab.Tactigen.com/ubermoitoring.io/umc-catalog). The **umc** catalog points to a git mirror of the **master** branch of this repo.
